-- mysql 数据库执行创建表该插件数据表的sql语句

-- 创建论坛用户的角色（权限），role
INSERT INTO role ( id, name, description) VALUES ( '2', '论坛用户', '用户网站自己的论坛用户');

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
--  Table structure for `plugin_bbs_post`
-- ----------------------------
DROP TABLE IF EXISTS `plugin_bbs_post`;
CREATE TABLE `plugin_bbs_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动编号',
  `classid` int(11) DEFAULT NULL COMMENT '论坛分类，对应post_class.id',
  `title` char(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '帖子标题',
  `view` int(11) DEFAULT NULL COMMENT '查看次数',
  `info` char(80) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '内容简介',
  `addtime` int(11) DEFAULT NULL COMMENT '发布时间',
  `userid` int(11) DEFAULT NULL COMMENT '帖子的发布用户，对应 user.id',
  `siteid` int(11) DEFAULT NULL COMMENT '该帖子属于哪个网站，站点，对应 site.id',
  `isdelete` tinyint(6) DEFAULT NULL,
  `comment_count` int(11) DEFAULT NULL COMMENT '回帖数',
  `last_comment_time` int(11) DEFAULT NULL COMMENT '最后回帖时间',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`,`view`,`userid`,`siteid`,`isdelete`,`title`,`comment_count`,`last_comment_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='论坛插件-帖子';

-- ----------------------------
--  Table structure for `plugin_bbs_post_class`
-- ----------------------------
DROP TABLE IF EXISTS `plugin_bbs_post_class`;
CREATE TABLE `plugin_bbs_post_class` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '分类名',
  `isdelete` tinyint(2) DEFAULT NULL COMMENT '是否已经被删除。0正常，1已删除，',
  `siteid` int(11) DEFAULT NULL COMMENT '该板块属于哪个网站，站点，对应 site.id',
  PRIMARY KEY (`id`),
  KEY `isdelete` (`isdelete`,`siteid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='论坛的分类，大类';

-- ----------------------------
--  Table structure for `plugin_bbs_post_comment`
-- ----------------------------
DROP TABLE IF EXISTS `plugin_bbs_post_comment`;
CREATE TABLE `plugin_bbs_post_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `postid` int(11) unsigned NOT NULL COMMENT 'post帖子的id',
  `addtime` int(11) unsigned NOT NULL COMMENT '发布时间',
  `userid` int(11) unsigned NOT NULL COMMENT '发布的用户',
  `text` varchar(2000) COLLATE utf8_unicode_ci NOT NULL COMMENT '回复的内容',
  `isdelete` tinyint(2) DEFAULT NULL COMMENT '是否已经被删除。0正常，1已删除，',
  `siteid` int(11) DEFAULT NULL COMMENT '该回复属于哪个网站，站点，对应 site.id',
  PRIMARY KEY (`id`),
  KEY `postid` (`postid`,`addtime`,`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='论坛帖子的回复（暂未用到，预留）';

-- ----------------------------
--  Table structure for `plugin_bbs_post_data`
-- ----------------------------
DROP TABLE IF EXISTS `plugin_bbs_post_data`;
CREATE TABLE `plugin_bbs_post_data` (
  `postid` int(11) unsigned NOT NULL COMMENT 'post的id',
  `text` text COLLATE utf8_unicode_ci NOT NULL COMMENT '帖子内容',
  PRIMARY KEY (`postid`),
  KEY `postid` (`postid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='发帖，帖子主表（post）的内容分表';

-- ----------------------------
--  Table structure for `plugin_bbs_system_set`
-- ----------------------------
DROP TABLE IF EXISTS `plugin_bbs_system_set`;
CREATE TABLE `plugin_bbs_system_set` (
  `id` int(11) NOT NULL COMMENT '对应 site.id',
  `name` char(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '论坛名',
  `is_reg` tinyint(1) DEFAULT NULL COMMENT '是否允许注册，1允许，0不允许注册',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='论坛设置';

SET FOREIGN_KEY_CHECKS = 1;

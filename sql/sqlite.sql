-- sqlite 数据库执行创建该插件数据表的sql语句

-- 创建论坛用户的角色（权限），role
INSERT INTO role ( id, name, description) VALUES ( '2', '论坛用户', '用户网站自己的论坛用户');

-- ----------------------------
--  Table structure for plugin_bbs_post
-- ----------------------------
DROP TABLE IF EXISTS "plugin_bbs_post";
CREATE TABLE "plugin_bbs_post" (
	 "id" integer PRIMARY KEY AUTOINCREMENT,
	 "classid" integer,
	 "title" varchar (255,0),
	 "view" integer,
	 "info" varchar (255,0),
	 "addtime" integer,
	 "userid" integer,
	 "isdelete" integer,
	 "comment_count" integer,
	 "siteid" integer
);

-- ----------------------------
--  Table structure for plugin_bbs_post_class
-- ----------------------------
DROP TABLE IF EXISTS "plugin_bbs_post_class";
CREATE TABLE "plugin_bbs_post_class" (
	 "id" integer PRIMARY KEY AUTOINCREMENT,
	 "name" varchar (255,0),
	 "isdelete" integer,
	 "siteid" integer
);

-- ----------------------------
--  Table structure for plugin_bbs_post_comment
-- ----------------------------
DROP TABLE IF EXISTS "plugin_bbs_post_comment";
CREATE TABLE "plugin_bbs_post_comment" (id integer PRIMARY KEY AUTOINCREMENT, postid integer, addtime integer, userid integer, text varchar (255), isdelete integer);

-- ----------------------------
--  Table structure for plugin_bbs_post_data
-- ----------------------------
DROP TABLE IF EXISTS "plugin_bbs_post_data";
CREATE TABLE "plugin_bbs_post_data" (postid integer PRIMARY KEY, text text);

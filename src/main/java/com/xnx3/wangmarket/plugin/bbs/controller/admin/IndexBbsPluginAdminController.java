package com.xnx3.wangmarket.plugin.bbs.controller.admin;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;

/**
 * 论坛管理后台首页,系统设置
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/bbs/admin/")
public class IndexBbsPluginAdminController extends BasePluginController {

	/**
	 * 管理后台首页
	 */
	@RequestMapping("index${url.suffix}")
	public String list(HttpServletRequest request,Model model){
		if(!haveSiteAuth()){
			return error(model, "无权限");
		}
		
		return "/plugin/bbs/admin/index";
	}
	
	
	@RequestMapping("welcome${url.suffix}")
	public String welcome(HttpServletRequest request,Model model){
		if(!haveSiteAuth()){
			return error(model, "无权限");
		}
		
		return "/plugin/bbs/admin/welcome";
	}
	
	
	
}

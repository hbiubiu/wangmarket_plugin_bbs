package com.xnx3.wangmarket.plugin.bbs.vo;

import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;

/**
 * 组合好的Post，包含 {@link Post}、 {@link PostData}
 * <br/>首先判断getResult()是否是 {@link BaseVO#SUCCESS}，若是，才可以调取其他的值。若不是，可通过getInfo()获取错误信息
 * @author 管雷鸣
 *
 */
public class PostVO extends BaseVO {
	private Post post;
	private User user;	//发帖用户
	private String text;	//帖子的内容
	private PostClass postClass;	//所属的栏目信息
	
	public Post getPost() {
		return post;
	}
	public void setPost(Post post) {
		this.post = post;
	}
	/**
	 * 发帖用户
	 * @return
	 */
	public User getUser() {
		return user;
	}
	/**
	 * 发帖用户
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * 帖子的内容
	 * @return
	 */
	public String getText() {
		return text;
	}
	/**
	 * 帖子的内容
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * 所属的栏目信息
	 * @return
	 */
	public PostClass getPostClass() {
		return postClass;
	}
	
	/**
	 * 所属的栏目信息
	 * @param postClass
	 */
	public void setPostClass(PostClass postClass) {
		this.postClass = postClass;
	}
	
}

package com.xnx3.wangmarket.plugin.bbs.controller;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xnx3.Lang;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.func.ActionLogCache;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.wangmarket.admin.controller.BaseController;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.service.PostService;
import com.xnx3.wangmarket.plugin.bbs.vo.PostListVO;
import com.xnx3.wangmarket.plugin.bbs.vo.PostVO;

/**
 * 论坛，帖子处理
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/bbs/")
public class PostBbsPluginController_ extends BaseController {
	@Resource
	private PostService postService;
	@Resource
	private SqlService sqlService;
	@Resource
	private UserService userService;
	
	/**
	 * 帖子列表
	 * 要么传入 siteid、要么传入classid
	 */
	@RequestMapping("list${url.suffix}")
	public String list(Post post,HttpServletRequest request,Model model){
		//判断是否传入了siteid,若没有传入，择要从classid中取
		String siteid = request.getParameter("siteid");
		if(siteid == null){
			if(request.getParameter("classid") == null){
				return error(model, "siteid、classid必须传入其中一个");
			}else{
				String classid = request.getParameter("classid");
				int cid = Lang.stringToInt(classid, 0);
				PostClass postClass = sqlService.findById(PostClass.class, cid);
				if(postClass == null){
					return error(model, "论坛板块不存在");
				}
				siteid = postClass.getSiteid()+"";
			}
		}
		
		Sql sql = new Sql(request);
		sql.setSearchColumn(new String[]{"classid=","title","view>","info","addtime","userid=", "siteid="});
		sql.appendWhere("isdelete = "+Post.ISDELETE_NORMAL);
		int count = sqlService.count("plugin_bbs_post", sql.getWhere());
		Page page = new Page(count, Global.getInt("LIST_EVERYPAGE_NUMBER"), request);
		sql.setSelectFromAndPage("SELECT plugin_bbs_post.*, user.nickname, user.head FROM plugin_bbs_post LEFT JOIN user ON user.id = plugin_bbs_post.userid ", page);
		sql.setOrderByField(new String[]{"id","last_comment_time","view"});
		sql.setDefaultOrderBy("plugin_bbs_post.id DESC");
		List<Map<String, Object>> list = sqlService.findMapBySql(sql);
		
		ActionLogCache.insert(request, "查看帖子列表");
		model.addAttribute("page", page);
		model.addAttribute("list", list);
		model.addAttribute("siteid", siteid);
		model.addAttribute("user", getUser());
		model.addAttribute("AttachmentFileNetUrl", AttachmentFile.netUrl());
		return "plugin/bbs/post/list";
	}
	
	/**
	 * 查看帖子详情
	 * @param post {@link Post}
	 */
	@RequestMapping("view${url.suffix}")
	public String view(@RequestParam(value = "id", required = true) int id,Model model, HttpServletRequest request){
		PostVO postVO = postService.read(id);
		if(postVO.getResult() == PostVO.SUCCESS){
			//查询回帖
			List commentList = postService.commentAndUser(postVO.getPost().getId(),10);
			model.addAttribute("postVO", postVO);
			model.addAttribute("commentList", commentList);
			model.addAttribute("user", getUser());	//当前登陆的用户
			model.addAttribute("AttachmentFileNetUrl", AttachmentFile.netUrl());
			
			ActionLogCache.insert(request, id, "查看帖子详情", postVO.getPost().getTitle());
			return "plugin/bbs/post/view";
		}else{
			ActionLogCache.insert(request, id, "查看帖子详情", "出错："+postVO.getInfo());
			return error(model, postVO.getInfo());
		}
	}
	
	/**
	 * 本周热门
	 * @param siteid 站点id，必须传入
	 * @param classid 分类板块的id，不传或者传入0，则是所有板块
	 */
	@RequestMapping("weekHot${url.suffix}")
	@ResponseBody
	public PostListVO weekHot(
			@RequestParam(value = "siteid", required = false, defaultValue="0") int siteid,
			@RequestParam(value = "classid", required = false, defaultValue="0") int classid){
		return postService.weekHot(siteid, classid);
	}
}

package com.xnx3.wangmarket.plugin.bbs;


/**
 * 总
 * @author 管雷鸣
 *
 */
public class Global {
	/**
	 * 当前版本
	 */
	public static final String VERSION = "1.0";
	
	/**
	 * 论坛用户自动注册的user的role权限
	 */
	public static final int BBS_USER_ROLE_ID = 2; 
	
	/**
	 * 论坛系统设置，设置的项存储在文件中的文件名，存储于 site/123/data/ATTACHMENT_JSON_FILE_NAME
	 */
	public static final String ATTACHMENT_JSON_FILE_NAME = "plugin_bbs_set.json";
	
	
}

package com.xnx3.wangmarket.plugin.bbs.controller.personal;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.Lang;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.func.ActionLogCache;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.func.Captcha;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.shiro.ShiroFunc;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.j2ee.vo.UploadFileVO;
import com.xnx3.wangmarket.admin.cache.KeFu;
import com.xnx3.wangmarket.admin.controller.BaseController;
import com.xnx3.wangmarket.admin.util.AliyunLog;
import com.xnx3.wangmarket.plugin.bbs.controller.BBSBaseController;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.service.PostService;
import com.xnx3.wangmarket.plugin.bbs.vo.PostVO;

/**
 * 论坛，帖子相关，用户登陆后的操作
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/bbs/personal/")
public class BbsPluginPersonalController extends BBSBaseController {
	@Resource
	private PostService postService;
	@Resource
	private SqlService sqlService;
	@Resource
	private UserService userService;

	
	/**
	 * 我的资料
	 */
	@RequestMapping("info${url.suffix}")
	public String info(Model model, HttpServletRequest request){
		User user = getUser();
		String classid = getRequestClassid(request);
		if(user == null){
			return error(model, "请先登陆", "plugin/bbs/login.do?siteid="+getRequestSiteid(request)+"&classid="+classid);
		}
		
		AliyunLog.addActionLog(user.getId(), "进入论坛我的资料信息页面", user.getUsername());
		
		model.addAttribute("head", userService.getHead("http://res.weiunity.com/image/default_head.png"));
		//可上传的后缀列表
		model.addAttribute("ossFileUploadImageSuffixList", Global.ossFileUploadImageSuffixList);
		//可上传的文件最大大小(KB)
		model.addAttribute("maxFileSizeKB", AttachmentFile.getMaxFileSizeKB());
		modelSet(request, model);
		return "plugin/bbs/personal/info";
	}

	
	/**
	 * 上传头像接口
	 */
	@RequestMapping(value="uploadHead${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public UploadFileVO uploadHead(Model model,HttpServletRequest request){
		User user = getUser();
		if(user == null){
			UploadFileVO vo = new UploadFileVO();
			vo.setBaseVO(UploadFileVO.FAILURE, "请先登陆");
			return vo;
		}
		
		
		UploadFileVO uploadFileVO = AttachmentFile.uploadImage("site/"+getSiteId()+"/plugin_bbs/", request, "image", 0);
		
		if(uploadFileVO.getResult() == UploadFileVO.SUCCESS){
			//上传成功，写日志
			AliyunLog.addActionLog(getSiteId(), "论坛插件上传头像成功："+uploadFileVO.getFileName(), uploadFileVO.getPath());
			
			User u = sqlService.findById(User.class, user.getId());
			u.setHead(uploadFileVO.getUrl());
			sqlService.save(u);
			setUser(u);
		}
		
		return uploadFileVO;
	}
	


	/**
	 * 修改昵称
	 * @param nickname get/post 传入要修改的昵称
	 */
	@RequestMapping("updateNickname${url.suffix}")
	@ResponseBody
	public BaseVO updateNickname(HttpServletRequest request){
		User user = getUser();
		if(user == null){
			return error("请先登陆");
		}
		
		BaseVO vo = userService.updateNickname(request);
		if(vo.getResult() - BaseVO.SUCCESS == 0){
			AliyunLog.addActionLog(getUserId(), "论坛中修改昵称", getUser().getNickname());
		}
		return vo;
	}
	
	

	/**
	 * 我的帖子列表
	 * 要传入 siteid
	 */
	@RequestMapping("postList${url.suffix}")
	public String postList(HttpServletRequest request,Model model){
		User user = getUser();
		if(user == null){
			return error(model, "请先登陆", "plugin/bbs/login.do?siteid="+getRequestSiteid(request));
		}
		
		Sql sql = new Sql(request);
		sql.setSearchColumn(new String[]{"classid=","title","view>","info","addtime","userid=", "siteid="});
		sql.appendWhere("isdelete = "+Post.ISDELETE_NORMAL);
		sql.appendWhere("userid = "+user.getId());
		int count = sqlService.count("plugin_bbs_post", sql.getWhere());
		Page page = new Page(count, Global.getInt("LIST_EVERYPAGE_NUMBER"), request);
		sql.setSelectFromAndPage("SELECT plugin_bbs_post.* FROM plugin_bbs_post ", page);
		sql.setOrderByField(new String[]{"id","last_comment_time","view"});
		sql.setDefaultOrderBy("plugin_bbs_post.id DESC");
		List<Map<String, Object>> list = sqlService.findMapBySql(sql);
		
		ActionLogCache.insert(request, "论坛插件查看我的帖子列表");
		model.addAttribute("page", page);
		model.addAttribute("list", list);
		modelSet(request, model);
		return "plugin/bbs/personal/postList";
	}
	
	

	/**
	 * 我的回帖列表
	 * 要传入 siteid
	 */
	@RequestMapping("postCommentList${url.suffix}")
	public String postCommentList(HttpServletRequest request,Model model){
		User user = getUser();
		if(user == null){
			return error(model, "请先登陆", "plugin/bbs/login.do?siteid="+getRequestSiteid(request));
		}
		
		Sql sql = new Sql(request);
		sql.setSearchColumn(new String[]{"postid="});
		sql.appendWhere("isdelete = "+Post.ISDELETE_NORMAL);
		sql.appendWhere("userid = "+user.getId());
		int count = sqlService.count("plugin_bbs_post_comment", sql.getWhere());
		Page page = new Page(count, Global.getInt("LIST_EVERYPAGE_NUMBER"), request);
		sql.setSelectFromAndPage("SELECT * FROM plugin_bbs_post_comment ", page);
		sql.setDefaultOrderBy("id DESC");
		List<Map<String, Object>> list = sqlService.findMapBySql(sql);
		
		ActionLogCache.insert(request, "论坛插件查看我的回帖列表");
		model.addAttribute("page", page);
		model.addAttribute("list", list);
		modelSet(request, model);
		return "plugin/bbs/personal/postCommentList";
	}
	
	
	
}

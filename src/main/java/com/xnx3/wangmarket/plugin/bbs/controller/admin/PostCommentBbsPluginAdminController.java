package com.xnx3.wangmarket.plugin.bbs.controller.admin;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.BaseEntity;
import com.xnx3.j2ee.func.ActionLogCache;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.util.AliyunLog;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.entity.PostComment;
import com.xnx3.wangmarket.plugin.bbs.generateCache.Bbs;
import com.xnx3.wangmarket.plugin.bbs.service.PostService;

/**
 * 论坛，回帖
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/bbs/admin/postComment/")
public class PostCommentBbsPluginAdminController extends BasePluginController {
	@Resource
	private PostService postService;
	@Resource
	private SqlService sqlService;

	/**
	 * 回帖列表
	 */
	@RequestMapping("list${url.suffix}")
	public String list(HttpServletRequest request,Model model){
		if(!haveSiteAuth()){
			return error(model, "无权限");
		}
		
		Sql sql = new Sql(request);
		sql.setSearchColumn(new String[]{"postid=","userid="});
		sql.appendWhere("plugin_bbs_post_comment.isdelete = "+Post.ISDELETE_NORMAL);
		sql.appendWhere("plugin_bbs_post_comment.siteid = "+getSiteId());
		int count = sqlService.count("plugin_bbs_post_comment", sql.getWhere());
		Page page = new Page(count, Global.getInt("LIST_EVERYPAGE_NUMBER"), request);
		sql.setSelectFromAndPage("SELECT plugin_bbs_post_comment.*, user.nickname FROM plugin_bbs_post_comment LEFT JOIN user ON user.id = plugin_bbs_post_comment.userid ", page);
		sql.setDefaultOrderBy("plugin_bbs_post_comment.id DESC");
		List<Map<String,Object>> list = sqlService.findMapBySql(sql);
		
		ActionLogCache.insert(request, "论坛插件管理后台查看回帖列表", "第"+page.getCurrentPageNumber()+"页");
		
		model.addAttribute("list", list);
		model.addAttribute("page", page);
		return "/plugin/bbs/admin/postComment/list";
	}
	
	/**
	 * 删除回帖
	 * @param id 回帖的id，plugin_bbs_post_comment.id
	 */
	@RequestMapping(value="deletePostComment${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO deletePostComment(@RequestParam(value = "id", required = true) int id, Model model, HttpServletRequest request){
		if(!haveSiteAuth()){
			return error("无权限");
		}
		Site site = getSite();
		
		PostComment pc = sqlService.findById(PostComment.class, id);
		if(pc!=null){
			//判断是否有权删除
			if(pc.getSiteid() - site.getId() != 0){
				return error("回帖不属于你，无权操作");
			}
			if(pc.getIsdelete() - Post.ISDELETE_NORMAL != 0){
				return error("回帖状态异常，无权操作");
			}
			
			pc.setIsdelete(BaseEntity.ISDELETE_DELETE);
			sqlService.save(pc);
			
			AliyunLog.addActionLog(pc.getId(), "论坛管理后台删除回帖成功", pc.getText());
			return success();
		}else{
			return error("回帖不存在");
		}
	}
	
	
}

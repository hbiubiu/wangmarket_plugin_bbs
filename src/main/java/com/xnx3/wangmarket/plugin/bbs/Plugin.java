package com.xnx3.wangmarket.plugin.bbs;

import com.xnx3.wangmarket.admin.pluginManage.anno.PluginRegister;

/**
 * 论坛，应用于网站后台
 * @author 管雷鸣
 */
@PluginRegister(id="bbs" , menuTitle = "论坛", menuHref="../../plugin/bbs/admin/index.do", applyToCMS=true)
public class Plugin{
	
}
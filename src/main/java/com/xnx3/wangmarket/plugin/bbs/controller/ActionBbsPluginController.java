package com.xnx3.wangmarket.plugin.bbs.controller;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xnx3.Lang;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.func.ActionLogCache;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.wangmarket.admin.controller.BaseController;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.service.PostService;
import com.xnx3.wangmarket.plugin.bbs.util.Action;
import com.xnx3.wangmarket.plugin.bbs.vo.ActionListVO;
import com.xnx3.wangmarket.plugin.bbs.vo.PostListVO;
import com.xnx3.wangmarket.plugin.bbs.vo.PostVO;

/**
 * 动作
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/bbs/")
public class ActionBbsPluginController extends BaseController {
	
	/**
	 * 最新动作列表接口
	 * @param siteid 传入要调取的动作的站点编号，拿这个站点（论坛）的最新动作
	 */
	@RequestMapping("action${url.suffix}")
	@ResponseBody
	public ActionListVO action(HttpServletRequest request,Model model,
			@RequestParam(value = "siteid", required = false, defaultValue="0") int siteid){
		ActionListVO vo = new ActionListVO();
		vo.setList(Action.getActionList(siteid));
		
		return vo;
	}
	
}

package com.xnx3.wangmarket.plugin.bbs.generateCache;

import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.generateCache.BaseGenerate;
import com.xnx3.wangmarket.admin.entity.Site;

/**
 * 系统相关
 * @author 管雷鸣
 *
 */
public class SystemSet extends BaseGenerate{
	
	/**
	 * 生成 isReg的js缓存，存入 site/123/data/...
	 * @param site
	 */
	public void isReg(Site site){
		
		if(site == null){
			return;
		}
		createCacheObject("isReg");
		cacheAdd(com.xnx3.wangmarket.plugin.bbs.entity.SystemSet.IS_REG_YES, "允许");
		cacheAdd(com.xnx3.wangmarket.plugin.bbs.entity.SystemSet.IS_REG_NO, "不允许");
		
		this.addCommonJsFunction();
		AttachmentFile.putStringFile("site/"+site.getId()+"/data/plugin_bbs_system_set_isReg.js", this.content);
		
	}
	
}

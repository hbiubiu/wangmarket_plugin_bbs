package com.xnx3.wangmarket.plugin.bbs.controller;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xnx3.BaseVO;
import com.xnx3.Lang;
import com.xnx3.StringUtil;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.func.ActionLogCache;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.wangmarket.admin.controller.BaseController;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.entity.PostComment;
import com.xnx3.wangmarket.plugin.bbs.service.PostService;
import com.xnx3.wangmarket.plugin.bbs.vo.PostCommentListVO;
import com.xnx3.wangmarket.plugin.bbs.vo.PostListVO;
import com.xnx3.wangmarket.plugin.bbs.vo.PostVO;

/**
 * 论坛，帖子处理
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/bbs/")
public class PostBbsPluginController extends BBSBaseController {
	@Resource
	private PostService postService;
	@Resource
	private SqlService sqlService;
	@Resource
	private UserService userService;
	
	/**
	 * 帖子列表
	 * 要么传入 siteid、要么传入classid
	 */
	@RequestMapping("list${url.suffix}")
	public String list(Post post,HttpServletRequest request,Model model){
		//判断是否传入了siteid,若没有传入，择要从classid中取
		String siteid = request.getParameter("siteid");
		if(siteid == null){
			if(request.getParameter("classid") == null){
				return error(model, "siteid、classid必须传入其中一个");
			}else{
				String classid = request.getParameter("classid");
				int cid = Lang.stringToInt(classid, 0);
				PostClass postClass = sqlService.findById(PostClass.class, cid);
				if(postClass == null){
					return error(model, "论坛板块不存在");
				}
				siteid = postClass.getSiteid()+"";
			}
		}
		
		Sql sql = new Sql(request);
		sql.setSearchColumn(new String[]{"classid=","title","view>","info","addtime","userid=", "siteid="});
		sql.appendWhere("isdelete = "+Post.ISDELETE_NORMAL);
		int count = sqlService.count("plugin_bbs_post", sql.getWhere());
		Page page = new Page(count, Global.getInt("LIST_EVERYPAGE_NUMBER"), request);
		sql.setSelectFromAndPage("SELECT plugin_bbs_post.*, user.nickname, user.head FROM plugin_bbs_post LEFT JOIN user ON user.id = plugin_bbs_post.userid ", page);
		sql.setOrderByField(new String[]{"id","last_comment_time","view"});
		sql.setDefaultOrderBy("plugin_bbs_post.id DESC");
		List<Map<String, Object>> list = sqlService.findMapBySql(sql);
		
		ActionLogCache.insert(request, "查看帖子列表");
		modelSet(request, model);
		model.addAttribute("page", page);
		model.addAttribute("list", list);
		model.addAttribute("siteid", siteid);
		return "plugin/bbs/post/list";
	}
	
	/**
	 * 查看帖子详情
	 * @param post {@link Post}
	 */
	@RequestMapping("view${url.suffix}")
	public String view(@RequestParam(value = "id", required = true) int id,Model model, HttpServletRequest request){
		PostVO postVO = postService.read(id);
		if(postVO.getResult() == PostVO.SUCCESS){
			//查询回帖
			List commentList = postService.commentAndUser(postVO.getPost().getId(),10);
			model.addAttribute("postVO", postVO);
			model.addAttribute("commentList", commentList);
			model.addAttribute("urltext", StringUtil.stringToUrl(postVO.getText()));
			model.addAttribute("utf8text", StringUtil.StringToUtf8(postVO.getText()));
			modelSet(request, model);
			model.addAttribute("siteid", postVO.getPost().getSiteid());
			
			ActionLogCache.insert(request, id, "查看帖子详情", postVO.getPost().getTitle());
			return "plugin/bbs/post/view";
		}else{
			ActionLogCache.insert(request, id, "查看帖子详情", "出错："+postVO.getInfo());
			return error(model, postVO.getInfo());
		}
	}
	
	/**
	 * 本周热门
	 * @param siteid 站点id，必须传入
	 * @param classid 分类板块的id，不传或者传入0，则是所有板块
	 */
	@RequestMapping("weekHot${url.suffix}")
	@ResponseBody
	public PostListVO weekHot(
			@RequestParam(value = "siteid", required = false, defaultValue="0") int siteid,
			@RequestParam(value = "classid", required = false, defaultValue="0") int classid){
		return postService.weekHot(siteid, classid);
	}
	
	/**
	 * 查看某个人的个人资料页面
	 * @param userid 要查看的是谁的个人资料
	 * @param siteid 站点编号
	 * @return
	 */
	@RequestMapping("home${url.suffix}")
	public String home(Model model, HttpServletRequest request,
			@RequestParam(value = "userid", required = false, defaultValue="0") int userid,
			@RequestParam(value = "siteid", required = false, defaultValue="0") int siteid){
		
		if(userid == 0){
			return error(model, "你要看谁的资料呢？");
		}
		User targetUser = sqlService.findById(User.class, userid);
		if(targetUser == null){
			return error(model, "查看的此人不存在");
		}
		
		model.addAttribute("targetUser", targetUser);	//查看的这个用户
		modelSet(request, model);
		return "plugin/bbs/home";
	}
	
	

	/**
	 * 获取帖子列表接口
	 * @param userid get传入userid，可以看到某个人的帖子，可以不填写，则是获取所有帖子
	 * @param siteid 必须传入，是哪个论坛
	 */
	@RequestMapping("postList${url.suffix}")
	@ResponseBody
	public PostListVO postList(HttpServletRequest request){
		PostListVO vo = new PostListVO();
		
		String siteidStr = getRequestSiteid(request);
		int siteid = 0;
		if(siteidStr.length() > 0){
			siteid = Lang.stringToInt(siteidStr, 0);
		}
		if(siteid < 1){
			vo.setBaseVO(BaseVO.FAILURE, "您是要调取哪个论坛的呢？请传入siteid");
			return vo;
		}
		
		Sql sql = new Sql(request);
		sql.setSearchColumn(new String[]{"classid=","title","view>","info","addtime","userid="});
		sql.appendWhere("isdelete = "+Post.ISDELETE_NORMAL);
		sql.appendWhere("siteid = "+siteid);
		int count = sqlService.count("plugin_bbs_post", sql.getWhere());
		Page page = new Page(count, Global.getInt("LIST_EVERYPAGE_NUMBER"), request);
		sql.setSelectFromAndPage("SELECT plugin_bbs_post.* FROM plugin_bbs_post ", page);
		sql.setOrderByField(new String[]{"id","last_comment_time","view"});
		sql.setDefaultOrderBy("plugin_bbs_post.id DESC");
		List<Post> list = sqlService.findBySql(sql, Post.class);
		
		ActionLogCache.insert(request, "论坛插件查看某人的帖子列表");
		vo.setList(list);
		vo.setPage(page);
		return vo;
	}
	

	/**
	 * 获取回帖列表
	 * @param userid get传入userid，可以看到某个人的回帖，可以不填写，则是获取所有回帖
	 * @param siteid 必须传入，是哪个论坛
	 */
	@RequestMapping("postCommentList${url.suffix}")
	@ResponseBody
	public PostCommentListVO postCommentList(HttpServletRequest request,Model model){
		PostCommentListVO vo = new PostCommentListVO();
		String siteidStr = getRequestSiteid(request);
		int siteid = 0;
		if(siteidStr.length() > 0){
			siteid = Lang.stringToInt(siteidStr, 0);
		}
		if(siteid < 1){
			vo.setBaseVO(BaseVO.FAILURE, "您是要调取哪个论坛的呢？请传入siteid");
			return vo;
		}
		
		Sql sql = new Sql(request);
		sql.setSearchColumn(new String[]{"postid=","userid="});
		sql.appendWhere("isdelete = "+Post.ISDELETE_NORMAL);
		sql.appendWhere("siteid = "+siteid);
		int count = sqlService.count("plugin_bbs_post_comment", sql.getWhere());
		Page page = new Page(count, Global.getInt("LIST_EVERYPAGE_NUMBER"), request);
		sql.setSelectFromAndPage("SELECT * FROM plugin_bbs_post_comment ", page);
		sql.setDefaultOrderBy("id DESC");
		List<PostComment> list = sqlService.findBySql(sql, PostComment.class);
		
		ActionLogCache.insert(request, "论坛插件查看某人的回帖列表");
		vo.setList(list);
		vo.setPage(page);
		return vo;
	}
	
	
}

<%@page import="com.xnx3.wangmarket.plugin.bbs.vo.PostVO"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

//标题
String title = request.getParameter("title");
if(title == null ){
	title = Global.get("SITE_NAME");
}else{
	title=title+"_"+Global.get("SITE_NAME");
}

//关键字
String keywords = request.getParameter("keywords");
if(keywords == null ){
	keywords = Global.get("SITE_KEYWORDS");
}

//描述
String description = request.getParameter("description");
if(description == null ){
	description = Global.get("SITE_DESCRIPTION");
}

//siteid
String siteid = "";
if(request.getParameter("siteid") != null && request.getParameter("siteid").length() > 0){
	//很多页面都有get传递这个参数
	siteid = request.getParameter("siteid");
}else if(request.getAttribute("siteid") != null){
	//list.do会有这个参数
	siteid = request.getAttribute("siteid").toString();
}else if(request.getAttribute("postVO") != null){
	//view.do 会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	siteid = postVO.getPost().getSiteid()+"";
}

//classid
String classid = "";
if(request.getParameter("classid") != null && request.getParameter("classid").length() > 0){
	//list.do 页面可能有这个参数
	classid = request.getParameter("classid");
}else if(request.getAttribute("postVO") != null){
	//view.do 页面会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	classid = postVO.getPost().getClassid()+"";
}

String postid = "";
if(request.getAttribute("postVO") != null){
	//view.do 页面会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	postid = postVO.getPost().getId()+"";
}

%><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><%=title %></title>
<meta name="keywords" content="<%=keywords %>" />
<meta name="description" content="<%=description %>" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="管雷鸣">

<!-- layer 、 layui -->
<link rel="stylesheet" href="http://res.weiunity.com/layui230/css/layui.css">
<script src="http://res.weiunity.com/layui230/layui.js"></script>
<script>
//加载 layer 模块
layui.use('layer', function(){
  layer = layui.layer;
});
</script>

<!-- 加载论坛设置数据 -->
<script src="${AttachmentFileNetUrl }site/${siteid }/data/plugin_bbs_system_set.js"></script>

<script src="http://res.weiunity.com/js/jquery-2.1.4.js"></script>

<!-- order by 列表的排序 -->
<script src="http://res.weiunity.com/js/iw.js"></script>

<style>
/*列表页头部form搜索框*/
.toubu_xnx3_search_form{
	padding-top:10px;
	padding-bottom: 10px;
}
/*列表页头部搜索，form里面的搜索按钮*/
.iw_list_search_submit{
	margin-left:22px;
}
/* 列表页，数据列表的table */
.iw_table{
	margin:0px;
}
/* 详情页，table列表详情展示，这里是描述，名字的td */
.iw_table_td_view_name{
	width:150px;
}
</style>

<link rel="stylesheet" href="<%=basePath %>plugin/bbs/css/global.css">
</head>
<body>


<style>
.fly-logo{
	color: white;
    font-size: 26px;
}
</style>
<div class="fly-header layui-bg-black">
  <div class="layui-container">
    <a class="fly-logo" href="<%=basePath %>plugin/bbs/list.do?siteid=<%=siteid %>" id="bbs_name">
      加载中..
    </a>
    <script> try{
    	document.getElementById("bbs_name").innerHTML = bbs['name'];
    }catch(e){} </script>
    <ul class="layui-nav fly-nav layui-hide-xs" id="topMenu" style="display:none;">
      <li class="layui-nav-item layui-this">
        <a href="<%=basePath %>plugin/bbs/personal/postList.do?siteid=<%=siteid %>"><i class="layui-icon">&#xe63c;</i>我发布的帖子</a>
      </li>
      <li class="layui-nav-item">
        <a href="<%=basePath %>plugin/bbs/personal/postCommentList.do?siteid=<%=siteid %>"><i class="layui-icon">&#xe655;</i>我回复的帖子</a>
      </li>
      <li class="layui-nav-item">
        <a href="<%=basePath %>plugin/bbs/addPost.do?siteid=<%=siteid %>&classid=<%=classid %>"><i class="layui-icon">&#xe61f;</i>发表新帖</a>
      </li>
    </ul>
    
    
    
    <ul class="layui-nav fly-nav-user" id="head_notLogin">
      <!-- 未登入的状态 -->
      <li class="layui-nav-item" >
        <a class="iconfont icon-touxiang layui-hide-xs" href="#"></a>
      </li>
      <li class="layui-nav-item">
        <a href="<%=basePath %>plugin/bbs/login.do?siteid=<%=siteid %>&classid=<%=classid %>&postid=<%=postid %>">登陆</a>
      </li>
      <li class="layui-nav-item">
        <a href="javascript:regJump();">注册</a>
      </li>
     </ul> 
      
     <ul class="layui-nav fly-nav-user" id="head_login"  style="display:none;"> 
      <!-- 登入后的状态 -->
      <li class="layui-nav-item">
        <a class="fly-nav-avatar" href="javascript:;">
          <cite class="layui-hide-xs">${user.nickname }</cite>
          <img src="${user.head }" onerror="this.src='http://res.weiunity.com/image/default_head.png'" />
        </a>
        <dl class="layui-nav-child">
          <dd><a href="<%=basePath %>plugin/bbs/personal/info.do?siteid=<%=siteid %>"><i class="layui-icon">&#xe612;</i>我的资料</a></dd>
          <dd><a href="<%=basePath %>plugin/bbs/personal/postList.do?siteid=<%=siteid %>"><i class="layui-icon">&#xe63c;</i>我的帖子</a></dd>
          <dd><a href="<%=basePath %>plugin/bbs/personal/postCommentList.do?siteid=<%=siteid %>"><i class="layui-icon">&#xe655;</i>我的回帖</a></dd>
          <hr style="margin: 5px 0;">
          <dd><a href="javascript:logout();" style="text-align: center;">退出</a></dd>
        </dl>
      </li>
    </ul>
    
    
  </div>
</div>
<script>
if('${user.id}' > 0){
  		//已登陆
  		document.getElementById('head_login').style.display='block';
  		document.getElementById('head_notLogin').style.display='none';
  		//最顶部的导航，如我的帖子、我的回帖等
  		document.getElementById('topMenu').style.display='block';
   }else{
	//未登陆
}
	
//注意：导航 依赖 element 模块，否则无法进行功能性操作
layui.use('element', function(){
  var element = layui.element;
});

//注销登陆
function logout(){
	iw.loading("注销中");    //显示“操作中”的等待提示
	$.post("<%=basePath %>plugin/bbs/logout.do?siteid=<%=siteid %>", function(data){
	    iw.loadClose();    //关闭“操作中”的等待提示
	    if(data.result == '1'){
	        iw.msgSuccess('退出登陆成功');
	        
	        //更改右上角的登陆标记
	        document.getElementById('head_login').style.display='none';
  			document.getElementById('head_notLogin').style.display='block';
	     }else if(data.result == '0'){
	         iw.msgFailure(data.info);
	     }else{
	         iw.msgFailure();
	     }
	});
}


</script>

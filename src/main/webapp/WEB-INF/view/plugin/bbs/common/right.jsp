<%@page import="com.xnx3.wangmarket.plugin.bbs.vo.PostVO"%>
<%@page import="com.xnx3.j2ee.util.Page"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

//siteid
String siteid = "";
if(request.getParameter("siteid") != null && request.getParameter("siteid").length() > 0){
	//很多页面都有get传递这个参数
	siteid = request.getParameter("siteid");
}else if(request.getAttribute("siteid") != null){
	//list.do会有这个参数
	siteid = request.getAttribute("siteid").toString();
}else if(request.getAttribute("postVO") != null){
	//view.do 会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	siteid = postVO.getPost().getSiteid()+"";
}

//classid
String classid = "";
if(request.getParameter("classid") != null && request.getParameter("classid").length() > 0){
	//list.do 页面可能有这个参数
	classid = request.getParameter("classid");
}else if(request.getAttribute("postVO") != null){
	//view.do 页面会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	classid = postVO.getPost().getClassid()+"";
}


%>


<!-- 右侧区域 -->
<dl class="fly-panel fly-list-one" id="action" style="min-height: 250px;">
  <dt class="fly-panel-title">最新动态</dt>
  
  <!-- 无数据时 -->
  <div class="fly-none">加载中</div>
</dl>
<script>
//加载最新动作
function loadAction(){
	$.post("<%=basePath %>plugin/bbs/action.do?siteid=<%=siteid %>", function(data){
	    var html = '';
	    if(data.result == '1'){
	    	if(data.list.length == 0){
	    		html = html + '<div class="fly-none">暂无动态</div>';
	    	}else{
		    	for(var i=0,l=data.list.length;i<l && i< 11;i++){
		        	html = '<dd>'+data.list[i]+'</dd>'+html;
	        	}
	    	}
	        
	    }else if(data.result == '0'){
	    	html = html + '<div class="fly-none">请传入站点编号</div>';
	    }else{
	    	html = html + '<div class="fly-none">获取失败</div>';
	    }
	    document.getElementById('action').innerHTML = '<dt class="fly-panel-title">最新动态</dt>'+html;
	});
}
loadAction();
</script>    



<dl class="fly-panel fly-list-one" id="weekHot" style="min-height: 250px;">
  <dt class="fly-panel-title">本周热贴</dt>
  
  <!-- 无数据时 -->
  <div class="fly-none">加载中</div>
</dl>
<script>
//加载本周热帖
function loadWeekHot(){
	$.post("<%=basePath %>plugin/bbs/weekHot.do?siteid=<%=siteid %>&classid=<%=classid %>", function(data){
	    var html = '<dt class="fly-panel-title">本周热贴</dt>';
	    if(data.result == '1'){
	    	if(data.list.length == 0){
	    		html = html + '<div class="fly-none">暂无动态</div>';
	    	}else{
				for(var i=0,l=data.list.length;i<l;i++){
		        	post = data.list[i];
		        	html = html + '<dd><a href="view.do?id='+post.id+'">'+post.title+'</a><span><i class="iconfont icon-pinglun1"></i>&nbsp;&nbsp;'+post.commentCount+'</span></dd>';
		        }	    	
	    	}
	        
	    }else if(data.result == '0'){
	    	html = html + '<div class="fly-none">请传入站点编号</div>';
	    }else{
	    	html = html + '<div class="fly-none">获取失败</div>';
	    }
	    document.getElementById('weekHot').innerHTML = html;
	});
}
loadWeekHot();
</script>

		
  
   <!--    
      <div class="fly-panel fly-link">
        <h3 class="fly-panel-title">友情链接</h3>
        <dl class="fly-panel-main">
          <dd><a href="http://www.wang.market/" target="_blank">网市场云建站系统</a><dd>
        </dl>
      </div>
	 -->

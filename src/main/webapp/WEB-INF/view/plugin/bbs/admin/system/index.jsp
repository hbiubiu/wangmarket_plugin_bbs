<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../../../../iw/common/head.jsp">
	<jsp:param name="title" value="文章列表"/>
</jsp:include>
<link href="<%=basePath %>css/site_two_subMenu.css" rel="stylesheet">

<div style="padding:10%;">
	<table class="layui-table" >
	  <tbody>
	    <tr>
	      <td style="width:120px;">论坛名字</td>
	      <td onclick="updateName();" style="cursor:pointer;">
	      	${systemSet.name }
	      	<i class="layui-icon layui-icon-edit" style="padding-left:10px;"></i>	
	      </td>
	    </tr>
	    <tr>
	      <td>是否开放注册</td>
	      <td>
	      	${systemSet.isReg }
	      </td>
	    </tr>
	  </tbody>
	</table>
</div>

<script>

//更改昵称
function updateName(){
	layer.prompt({
	  formType: 0,
	  value: '${systemSet.name}',
	  title: '修改论坛名字'
	}, function(value, index, elem){
		layer.close(index);
		parent.iw.loading("修改中");
		
		$.post("save.do", {"name":"name" ,"value":value }, function(data){
			 	parent.iw.loadClose();
			 	if(data.result == '1'){
			 		parent.iw.msgSuccess("修改成功");
			 		location.reload();
			  	}else if(data.result == '0'){
			  		parent.iw.msgFailure(data.info);
			  	}else{
			  		parent.iw.msgFailure();
			  	}
		    }, 
		"json");
	});

}

</script>

</body>
</html>
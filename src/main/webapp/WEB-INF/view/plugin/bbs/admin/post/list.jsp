<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../../../../iw/common/head.jsp">
	<jsp:param name="title" value="查看帖子列表"/>
</jsp:include>

<jsp:include page="../../../../iw/common/list/formSearch_formStart.jsp" ></jsp:include>
	<jsp:include page="../../../../iw/common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="帖子标题"/>
		<jsp:param name="iw_name" value="title"/>
	</jsp:include>
	
    <input class="layui-btn iw_list_search_submit" type="submit" value="搜索" />
</form>

<table class="layui-table iw_table">
  <thead>
    <tr>
    	<th style="width:60px;">id</th>
    	<th>帖子标题</th>
		<th style="width:100px;">作者</th>
		<th style="width:30px;">阅读</th>
		<th style="width:30px;">回复</th>
        <th style="width:110px;">时间</th>
        <th style="width:40px;">操作</th>
    </tr> 
  </thead>
  <tbody>
  	<c:forEach items="${list}" var="post">
	   	<tr>
	        <td>${post.id }</td>
	        <td style="cursor: pointer;" onclick="window.open('<%=basePath %>plugin/bbs/view.do?id=${post.id }');">${post.title }</td>
	        <td>${post.nickname }</td>
	        <td>${post.view }</td>
	        <td>${post.comment_count }</td>
	        <td><x:time linuxTime="${post['addtime'] }" format="yy-MM-dd HH:mm"></x:time></td>
	        <td>
	        	<botton class="layui-btn layui-btn-sm" onclick="deletePost(${post.id }, '${post.title }');" style="margin-left: 3px;"><i class="layui-icon">&#xe640;</i></botton>
	        </td>
	    </tr>
	</c:forEach>
  </tbody>
</table>
<!-- 通用分页跳转 -->
<jsp:include page="../../../../iw/common/page.jsp"></jsp:include>
<div style="padding: 20px;color: gray;">
	<div>操作按钮提示:</div>
	<!-- <div><i class="layui-icon">&#xe642;</i> &nbsp;：编辑操作，进行修改</div> -->
	<div><i class="layui-icon">&#xe640;</i> &nbsp;：删除操作，删除某项</div>
</div>


<script type="text/javascript">
//根据id删除帖子
function deletePost(id,name){
	var dtp_confirm = layer.confirm('确定要删除帖子“'+name+'”？', {
	  btn: ['删除','取消'] //按钮
	}, function(){
		layer.close(dtp_confirm);
		
		parent.iw.loading("删除中");    //显示“操作中”的等待提示
		$.post('deletePost.do?id='+id, function(data){
		    parent.iw.loadClose();    //关闭“操作中”的等待提示
		    if(data.result == '1'){
		        parent.iw.msgSuccess('删除成功');
		        window.location.reload();	//刷新当前页
		     }else if(data.result == '0'){
		         parent.iw.msgFailure(data.info);
		     }else{
		         parent.iw.msgFailure();
		     }
		});
		
	}, function(){
	});
}

</script>

<jsp:include page="../../../../iw/common/foot.jsp"></jsp:include> 
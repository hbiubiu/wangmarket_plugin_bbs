<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/head.jsp">
	<jsp:param name="title" value="${postVO.post.title }"/>
</jsp:include>
<script type="text/javascript" src="${AttachmentFileNetUrl }site/${postVO.post.siteid }/data/plugin_bbs_postClass.js"></script>
<script type="text/javascript" src="http://res.weiunity.com/js/xss.js"></script>





<div class="layui-hide-xs">
  <div class="fly-panel fly-column">
    <div class="layui-container">
      <ul class="layui-clear">
        <li class="layui-hide-xs"><a href="list.do?siteid=${postVO.post.siteid }">所有帖子</a></li>
<script>
try{
	for (var p in postClass) {
        document.write('<li id="class_'+p+'"><a href="list.do?siteid=${postVO.post.siteid }&classid='+p+'">'+postClass[p]+'</a></li>');
    }
    document.getElementById('class_${postVO.post.classid }').setAttribute("class", "layui-this");
}catch(e){
	console.log(e);
}
</script>        
        
        <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><span class="fly-mid"></span></li> 
        
      </ul> 
    </div>
  </div>
</div>

<div class="layui-container">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md8 content detail">
      <div class="fly-panel detail-box">
        <h1>${postVO.post.title }</h1>
        <div class="fly-detail-info" style=" position: inherit; height: 0px; line-height: 90px; margin-right: 30px;">
          
          <span class="layui-badge"></span>
          
          
          <span class="fly-list-nums" style="margin-right: 30px;"> 
            <a href="#comment"><i class="iconfont" title="回复">&#xe60c;</i> ${postVO.post.commentCount }</a>
            <i class="iconfont" title="人气">&#xe60b;</i> ${postVO.post.view }
          </span>
        </div>
        <div class="detail-about">
          <a class="fly-avatar" href="home.do?siteid=${postVO.post.siteid }&userid=${postVO.user.id }">
            <img src="<%=basePath %>upload/userHead/${postVO.user.head }" alt="${postVO.user.nickname }" onerror="this.src='http://res.weiunity.com/image/default_head.png'" style="width: 22px;height: 22px;" />
          </a>
          <div class="fly-detail-user">
            <a href="home.do?siteid=${postVO.post.siteid }&userid=${postVO.user.id }" class="fly-link">
              <cite>${postVO.user.nickname }</cite>
            </a>
            <span><x:time linuxTime="${postVO.post.addtime }"></x:time></span>
          </div>
        </div>
        <div class="detail-body photos" id="text">
        	<!-- url编码的内容 -->
        	<textarea id="urltext" style="display:none;">${utf8text }</textarea>
          	加载中...
        </div>
      </div>

      <div class="fly-panel detail-box" id="flyReply">
      <a name="comment"></a>
        <fieldset class="layui-elem-field layui-field-title" style="text-align: center;">
          <legend>回帖</legend>
        </fieldset>
        <ul class="jieda" id="jieda">
        
        
        
		<c:forEach items="${commentList}" var="postComment">
		
			<li class="jieda-daan">
	            <div class="detail-about detail-about-reply">
	              <a class="fly-avatar" href="">
	                <img src="<%=basePath %>upload/userHead/${postComment['head'] }" alt="${postComment['nickname'] }" onerror="this.src='http://res.weiunity.com/image/default_head.png'" style="width:20px; height: 20px;" />
	              </a>
	              <div class="fly-detail-user">
	                <a href="" class="fly-link">
	                  <cite>${postComment['nickname'] }</cite>
	                  回复于 <x:time linuxTime="${postComment['addtime'] }"></x:time>              
	                </a>
	              </div>
	            </div>
	            <div class="detail-body jieda-body photos">
	              ${postComment['text'] }
	            </div>
	          </li>
	          
		</c:forEach>
		
          
          <!-- 无数据时 -->
          <!-- <li class="fly-none">消灭零回复</li> -->
        </ul>
        
        <div id="apply_login_panel" class="layui-form layui-form-pane" style="border-style: solid; border-color: #e6e6e6;border-width: 1px;padding: 50px;">
	        <div class="layui-form-item" style="text-align:center;">
	        	<div style="color: gray; padding-bottom: 20px;">您需要登陆之后后才能进行回复～～～～～</div>
	        	<a href="login.do?siteid=${postVO.post.siteid }&classid=${postVO.post.classid }&postid=${postVO.post.id }" class="layui-btn layui-btn-lg" style="margin-right:20px;">立即登陆</a>
	        	<a href="javascript:regJump();" class="layui-btn layui-btn-primary  layui-btn-lg"  style="margin-left:20px;">免费注册</a>
	        </div>
        </div>
        
        <!-- 默认先让回复的这个隐藏 -->
        <div id="apply_form_panel" style="display:none;" class="layui-form layui-form-pane">
          <form action="" method="post">
          	<input type="hidden" name="postid" value="${postVO.post.id }" />
            <div class="layui-form-item layui-form-text">
              <div class="layui-input-block">
                <textarea id="text" name="text" required lay-verify="required" placeholder="请输入2000字以内的回复内容"  class="layui-textarea fly-editor" style="height: 150px;"></textarea>
              </div>
            </div>
            
            <div class="layui-form-item">
                <label for="L_vercode" class="layui-form-label">验证</label>
                <div class="layui-input-inline">
                  <input type="text" id="code" name="code" required lay-verify="required" placeholder="请输入右边的验证码" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid yzm">
                  <span style="color: #c00;">
                  	<img id="codeImg" src="<%=basePath %>/captcha.do" onclick="reloadCode();" style="height: 22px;width: 110px; cursor: pointer;"/>
                  </span>
                </div>
                
                <button class="layui-btn" lay-submit="" lay-filter="demo1" style="margin-left:20px;">立即发布</button>
              </div>
            
          </form>
        </div>
        
        <script>
        	if('${user.id}' > 0){
        		//已登陆
        		document.getElementById('apply_form_panel').style.display='block';
        		document.getElementById('apply_login_panel').style.display='none';
        	}else{
				//未登陆
			}
        </script>
        
        
      </div>
    </div>
    <div class="layui-col-md4">
      <jsp:include page="../common/right.jsp"></jsp:include> 
    </div>
  </div>
</div>





<script type="text/javascript">
//重新加载验证码
function reloadCode(){
	var code=document.getElementById('codeImg');
	code.setAttribute('src','<%=basePath %>captcha.do?'+Math.random());
	//这里必须加入随机数不然地址相同我发重新加载
}


layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form;
  //监听提交
	form.on('submit(demo1)', function(data){
		iw.loading('保存中');
		var d=$("form").serialize();
        $.post("addCommentSubmit.do", d, function (result) { 
        	iw.loadClose();
        	var obj = JSON.parse(result);
        	if(obj.result == '1'){
        		iw.msgSuccess("发布成功");
        		window.location.reload();
        	}else if(obj.result == '0'){
        		reloadCode();
        		layer.msg(obj.info, {shade: 0.3})
        	}else{
        		reloadCode();
        		layer.msg(result, {shade: 0.3})
        	}
         }, "text");
		
		return false;
  });
  
});
</script>


<script>
layui.use('util', function(){
  var util = layui.util;
  var urltext = unescape(document.getElementById('urltext').value);
  
  document.getElementById('text').innerHTML = filterXSS(unescape(urltext.replace(/\\/g,'%'))); 
});

</script>


<jsp:include page="../common/foot.jsp"></jsp:include>
<%@page import="com.xnx3.wangmarket.plugin.bbs.vo.PostVO"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

//siteid
String siteid = "";
if(request.getParameter("siteid") != null && request.getParameter("siteid").length() > 0){
	//很多页面都有get传递这个参数
	siteid = request.getParameter("siteid");
}else if(request.getAttribute("siteid") != null){
	//list.do会有这个参数
	siteid = request.getAttribute("siteid").toString();
}else if(request.getAttribute("postVO") != null){
	//view.do 会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	siteid = postVO.getPost().getSiteid()+"";
}

%>
<jsp:include page="common/head.jsp">
   	<jsp:param name="title" value="查看${targetUser.nickname }的个人资料"/>
</jsp:include>
<script type="text/javascript" src="${AttachmentFileNetUrl }site/${siteid }/data/plugin_bbs_postClass.js"></script>
<link rel="stylesheet" href="<%=basePath %>plugin/bbs/css/global.css">




<div class="fly-home fly-panel" style="background-image: url();">
  <img src="${targetUser.head }" alt="${targetUser.nickname }" onerror="this.src='http://res.weiunity.com/image/default_head.png'">
  <h1>
    ${targetUser.nickname }
    <!-- <i class="iconfont icon-nv"></i>  -->
    <!--
    <span style="color:#c00;">（管理员）</span>
    <span style="color:#5FB878;">（社区之光）</span>
    <span>（该号已被封）</span>
    -->
  </h1>

  <p class="fly-home-info" style="padding-top:8px;">
    <i class="iconfont icon-shijian"></i><span><x:time linuxTime="${targetUser['regtime'] }" format="yy-MM-dd"></x:time>&nbsp; 加入</span>
    &nbsp;&nbsp;
    <i class="iconfont icon-shijian"></i><span><x:time linuxTime="${targetUser['lasttime'] }" format="yy-MM-dd"></x:time>&nbsp; 最后登陆</span>
  </p>

</div>

<div class="layui-container">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md6 fly-home-jie">
      <div class="fly-panel">
        <h3 class="fly-panel-title">最近发布的帖子</h3>
        <ul class="jie-row" id="post">
          <div class="fly-none">加载中...</div>
          <!-- <div class="fly-none" style="min-height: 50px; padding:30px 0; height:auto;"><i style="font-size:14px;">没有发表任何求解</i></div> -->
        </ul>
      </div>
    </div>

<script>
//时间戳转时间
function formatDateTime(timeStamp) { 
    var date = new Date();
    date.setTime(timeStamp * 1000);
    var y = date.getFullYear();    
    var m = date.getMonth() + 1;    
    m = m < 10 ? ('0' + m) : m;    
    var d = date.getDate();    
    d = d < 10 ? ('0' + d) : d;    
    var h = date.getHours();  
    h = h < 10 ? ('0' + h) : h;  
    var minute = date.getMinutes();  
    var second = date.getSeconds();  
    minute = minute < 10 ? ('0' + minute) : minute;    
    second = second < 10 ? ('0' + second) : second;   
    return y + '-' + m + '-' + d;    
};  


//加载本周热帖
function post(){
	$.post("<%=basePath %>plugin/bbs/postList.do?siteid=<%=siteid %>&userid=${targetUser.id}", function(data){
	    var html = '';
	    if(data.result == '1'){
	    	if(data.list.length == 0){
	    		html = html + '<div class="fly-none">尚无发帖</div>';
	    	}else{
				for(var i=0,l=data.list.length;i<l;i++){
		        	post = data.list[i];
		        	html = html + '<li><a href="view.do?id='+post.id+'" class="jie-title">'+post.title+'</a><i>'+formatDateTime(post.addtime)+'</i><em class="layui-hide-xs">'+post.view+'阅/'+post.commentCount+'答</em></li>';
		        }	    	
	    	}
	        
	    }else if(data.result == '0'){
	    	html = html + '<div class="fly-none">'+data.info+'</div>';
	    }else{
	    	html = html + '<div class="fly-none">获取失败</div>';
	    }
	    document.getElementById('post').innerHTML = html;
	});
}
post();
</script>
    
    <div class="layui-col-md6 fly-home-da" style="min-height: 500px;">
      <div class="fly-panel" style="min-height: 500px;">
        <h3 class="fly-panel-title">ta最近的回帖</h3>
        <ul class="jie-row" id="postComment" style="padding: 5px 20px;">
          <div class="fly-none">加载中...</div>
          <!-- <div class="fly-none" style="min-height: 50px; padding:30px 0; height:auto;"><i style="font-size:14px;">没有发表任何求解</i></div> -->
        </ul>
      </div>
    </div>
  </div>
</div>

<script>
//加载最新评论
function postComment(){
	$.post("<%=basePath %>plugin/bbs/postCommentList.do?siteid=<%=siteid %>&userid=${targetUser.id}", function(data){
	    var html = '';
	    if(data.result == '1'){
	    	if(data.list.length == 0){
	    		html = html + '<div class="fly-none">尚无回帖</div>';
	    	}else{
				for(var i=0,l=data.list.length;i<l;i++){
		        	post = data.list[i];
		        	html = html + '<li><a href="view.do?id='+post.postid+'" class="jie-title" style="max-width: 82%;">'+post.text+'</a><i>'+formatDateTime(post.addtime)+'</i></li>';
		        }	    	
	    	}
	        
	    }else if(data.result == '0'){
	    	html = html + '<div class="fly-none">'+data.info+'</div>';
	    }else{
	    	html = html + '<div class="fly-none">获取失败</div>';
	    }
	    document.getElementById('postComment').innerHTML = html;
	});
}
postComment();
</script>

				
<jsp:include page="common/foot.jsp"></jsp:include> 
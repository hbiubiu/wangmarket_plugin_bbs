<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/head.jsp">
   	<jsp:param name="title" value="个人资料设置"/>
</jsp:include>
<script type="text/javascript" src="${AttachmentFileNetUrl }site/${siteid }/data/plugin_bbs_postClass.js"></script>
<link rel="stylesheet" href="<%=basePath %>plugin/bbs/css/global.css">






<div class="layui-container fly-marginTop fly-user-main">
  <jsp:include page="common/leftMenu.jsp"></jsp:include> 
	<script>
		//设置选中的菜单
		document.getElementById('personal_leftMenu_post').setAttribute("class", "layui-nav-item layui-this"); 
	</script>
  <div class="site-tree-mobile layui-hide">
    <i class="layui-icon">&#xe602;</i>
  </div>
  <div class="site-mobile-shade"></div>
  
  <div class="site-tree-mobile layui-hide">
    <i class="layui-icon">&#xe602;</i>
  </div>
  <div class="site-mobile-shade"></div>
  
  
  <div class="fly-panel fly-panel-user" pad20>
    <div class="layui-tab layui-tab-brief" lay-filter="user">
      <ul class="layui-tab-title" id="LAY_mine">
        <li class="layui-this" lay-id="info">我的帖子</li>
      </ul>
      <div class="layui-tab-content" style="padding: 20px 0;">
        
        
        <div class="layui-tab-item layui-show">
<ul class="mine-view jie-row">
<c:forEach items="${list}" var="post">
	<li>
      <a class="jie-title" href="../view.do?id=${post.id }" target="_blank">${post.title }</a>
      <i><x:time linuxTime="${post['addtime'] }" format="yy-MM-dd HH:mm"></x:time></i>
      <!-- <a class="mine-edit" href="/jie/edit/8116">编辑</a> -->
      <em>${post.view }阅/${post.comment_count }答</em>
    </li>
</c:forEach>          
</ul>
          <div id="LAY_page">
        	<jsp:include page="../../../iw/common/page.jsp"></jsp:include>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>


<script>
layui.use('upload', function(){
	var upload = layui.upload;
	//上传图片
	upload.render({
		elem: '#uploadImages' //绑定元素
		,url: 'uploadHead.do' //上传接口
		,field: 'image'
		,accept: 'file'
		,size: ${maxFileSizeKB}
		,exts:'${ossFileUploadImageSuffixList }'	//可上传的文件后缀
		,done: function(res){
			//上传完毕回调
			loadClose();
			if(res.result == 1){
				try{
					document.getElementById("head").src = res.url;
				}catch(err){}
				parent.iw.msgSuccess("上传成功");
			}else{
				parent.iw.msgFailure(res.info);
			}
		}
		,error: function(index, upload){
			//请求异常回调
			parent.iw.loadClose();
			parent.iw.msgFailure();
		}
		,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
			parent.iw.loading('上传中..');
		}
	});
});


//更改昵称
function updateNickname(){
	layer.prompt({
	  formType: 0,
	  value: '${user.nickname}',
	  title: '更改昵称，限15字以内'
	}, function(value, index, elem){
		layer.close(index);
		iw.loading("修改中");
		
		$.post("updateNickname.do", {"nickname":value }, function(data){
			 	iw.loadClose();
			 	if(data.result == '1'){
			 		iw.msgSuccess("修改成功");
			 		document.getElementById("nickname").innerHTML = data.info;
			  	}else if(data.result == '0'){
			  		iw.msgFailure(data.info);
			  	}else{
			  		iw.msgFailure();
			  	}
		    }, 
		"json");
	});

}

</script>

				
<jsp:include page="../common/foot.jsp"></jsp:include> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
<html>
<head>
	<jsp:include page="../common/head.jsp">
    	<jsp:param name="title" value="发帖"/>
    </jsp:include>
    <script type="text/javascript" src="${AttachmentFileNetUrl }site/<%=request.getParameter("siteid") %>/data/plugin_bbs_postClass.js"></script>
</head>
<body>

<div class="layui-hide-xs">
  <div class="fly-panel fly-column">
    <div class="layui-container">
      <ul class="layui-clear">
        <li class="layui-hide-xs"><a href="list.do?siteid=<%=request.getParameter("siteid") %>">所有帖子</a></li>
<script>
try{
	for (var p in postClass) {
        document.write('<li><a href="list.do?siteid=<%=request.getParameter("siteid") %>&classid='+p+'">'+postClass[p]+'</a></li>');
    }
}catch(e){
	console.log(e);
}
</script>        
      </ul> 
    </div>
  </div>
</div>



<div class="layui-container fly-marginTop">
  <div class="fly-panel" pad20 style="padding-top: 5px;">
    <!--<div class="fly-none">没有权限</div>-->
    <div class="layui-form layui-form-pane">
      <div class="layui-tab layui-tab-brief" lay-filter="user">
        <ul class="layui-tab-title">
          <li class="layui-this">发表新帖<!-- 编辑帖子 --></li>
        </ul>
        <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
          <div class="layui-tab-item layui-show">
          
          
            <form action="" method="post">
            	<input type="hidden" id="siteid" name="siteid" value="<%=request.getParameter("siteid") %>" />
              <div class="layui-row layui-col-space15 layui-form-item">
                <div class="layui-col-md3">
                  <label class="layui-form-label">所在专栏</label>
                  <div class="layui-input-block">
                    <style>
                    	.layui-form-select dl{
                    		z-index:1000;
                    	}
                    </style>
                    <script type="text/javascript">writeSelectAllOptionForpostClass_('<%=request.getParameter("classid") %>','请选择', true);</script>

                  </div>
                </div>
                <div class="layui-col-md9">
                  <label for="L_title" class="layui-form-label">标题</label>
                  <div class="layui-input-block">
                    <input type="text" id="L_title" name="title" required lay-verify="required" autocomplete="off" class="layui-input">
                    <!-- <input type="hidden" name="id" value="{{d.edit.id}}"> -->
                  </div>
                </div>
              </div>
              <div class="layui-form-item layui-form-text">
                <div class="layui-input-block">
                	<textarea id="text" name="text" required lay-verify="required" placeholder="详细描述" style="height: 260px; width:100%; border-color: #e6e6e6;"></textarea>
                </div>
              </div>
              
              <div class="layui-form-item">
                <label for="L_vercode" class="layui-form-label">人类验证</label>
                <div class="layui-input-inline">
                  <input type="text" id="code" name="code" required lay-verify="required" placeholder="请输入右边的验证码" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid yzm">
                  <span style="color: #c00;">
                  	<img id="codeImg" src="<%=basePath %>/captcha.do" onclick="reloadCode();" style="height: 22px;width: 110px; cursor: pointer;"/>
                  </span>
                </div>
              </div>
              <div class="layui-form-item">
                <button class="layui-btn" lay-submit="" lay-filter="demo1">立即发布</button>
              </div>
            </form>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
//重新加载验证码
function reloadCode(){
	var code=document.getElementById('codeImg');
	code.setAttribute('src','<%=basePath %>captcha.do?'+Math.random());
	//这里必须加入随机数不然地址相同我发重新加载
}


layui.use(['form'], function(){
  var form = layui.form;
  //监听提交
	form.on('submit(demo1)', function(data){
		iw.loading('保存中');
		var d=$("form").serialize();
        $.post("addPostSubmit.do", d, function (result) { 
        	iw.loadClose();
        	var obj = JSON.parse(result);
        	if(obj.result == '1'){
        		iw.msgSuccess("发布成功");
        		window.location.href='<%=basePath %>plugin/bbs/view.do?id='+obj.info;
        	}else if(obj.result == '0'){
        		reloadCode();
        		layer.msg(obj.info, {shade: 0.3})
        	}else{
        		reloadCode();
        		layer.msg(result, {shade: 0.3})
        	}
         }, "text");
		
		return false;
  });
  
});

</script>

<!-- 配置文件 -->
<script type="text/javascript" src="<%=basePath %>module/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="<%=basePath %>module/ueditor/ueditor.all.js"></script>
<!-- 实例化编辑器 -->
<script type="text/javascript">
var ueditorText = document.getElementById('text').innerHTML;
var ue = UE.getEditor('text',{
	autoHeightEnabled: true,
	autoFloatEnabled: true,
	initialFrameHeight:460,
	toolbars: [
	    ['fullscreen', 'bold', 'indent', 'italic', 'source', 'fontfamily', 'fontsize', 'simpleupload', 'link', 'emotion', 'spechars', 'map', 'justifyleft', 'justifyright', 'justifycenter', 'forecolor', 'inserttable']
	]
});
//对编辑器的操作最好在编辑器ready之后再做
ue.ready(function() {
	document.getElementById("myEditor").style.height='auto';
});
</script>



<jsp:include page="../common/foot.jsp"></jsp:include>
<%@page import="com.xnx3.wangmarket.plugin.bbs.vo.PostVO"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

//标题
String title = request.getParameter("title");
if(title == null ){
	title = Global.get("SITE_NAME");
}else{
	title=title+"_"+Global.get("SITE_NAME");
}

//关键字
String keywords = request.getParameter("keywords");
if(keywords == null ){
	keywords = Global.get("SITE_KEYWORDS");
}

//描述
String description = request.getParameter("description");
if(description == null ){
	description = Global.get("SITE_DESCRIPTION");
}

//siteid
String siteid = "";
if(request.getParameter("siteid") != null && request.getParameter("siteid").length() > 0){
	//很多页面都有get传递这个参数
	siteid = request.getParameter("siteid");
}else if(request.getAttribute("siteid") != null){
	//list.do会有这个参数
	siteid = request.getAttribute("siteid").toString();
}else if(request.getAttribute("postVO") != null){
	//view.do 会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	siteid = postVO.getPost().getSiteid()+"";
}

//classid
String classid = "";
if(request.getParameter("classid") != null && request.getParameter("classid").length() > 0){
	//list.do 页面可能有这个参数
	classid = request.getParameter("classid");
}else if(request.getAttribute("postVO") != null){
	//view.do 页面会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	classid = postVO.getPost().getClassid()+"";
}

String postid = "";
if(request.getAttribute("postVO") != null){
	//view.do 页面会有这个参数
	PostVO postVO = (PostVO) request.getAttribute("postVO");
	postid = postVO.getPost().getId()+"";
}

%>
<ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
    <li class="layui-nav-item" id="personal_leftMenu_my">
      <a href="<%=basePath %>plugin/bbs/personal/info.do?siteid=<%=siteid %>">
        <i class="layui-icon">&#xe612;</i>
        我的资料
      </a>
    </li>
    <li class="layui-nav-item" id="personal_leftMenu_post">
      <a href="<%=basePath %>plugin/bbs/personal/postList.do?siteid=<%=siteid %>">
        <i class="layui-icon">&#xe63c;</i>
        我的帖子
      </a>
    </li>
    <li class="layui-nav-item" id="personal_leftMenu_postComment">
      <a href="<%=basePath %>plugin/bbs/personal/postCommentList.do?siteid=<%=siteid %>">
        <i class="layui-icon">&#xe655;</i>
        我的回帖
      </a>
    </li>
    
    
  </ul>

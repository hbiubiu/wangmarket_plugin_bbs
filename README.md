# wangmarket_plugin_bbs

wangmarket 网市场云建站系统之 论坛插件。


## 二次开发
1. 首先导入主项目，也就是我们网市场云建站系统 https://gitee.com/mail_osc/wangmarket 将此项目导入，能运行起来
1. 下载本项目，将 /src/ 目录下的文件，按照目录结构，粘贴入 网市场云建站系统 的主项目中。
1. 数据库执行创建表语句 [/sql/mysql.sql](https://gitee.com/mail_osc/wangmarket_plugin_bbs/blob/master/sql/mysql.sql)
1. 再次运行项目，随便登陆个网站管理后台（默认运行起来后，会自带一个默认账号密码都是 wangzhan 的账号，可以用此账号登陆）
1. 登陆网站管理后台后，找到左侧的 功能插件 下的 论坛，即可。


